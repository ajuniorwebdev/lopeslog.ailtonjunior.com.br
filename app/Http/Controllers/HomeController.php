<?php

namespace App\Http\Controllers;

use Exception;

use App\Mail\ContactMail;
use App\Mail\ContactMailGreetings;
use App\Mail\BudgetMail;
use App\Mail\BudgetMailGreetings;
use App\Mail\NewsletterMail;
use App\Mail\NewsletterMailGreetings;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\File;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function sendContact(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'email|required',
            'phone' => 'required',
            'message' => 'required'
        ]);

        try {

            Mail::to('a.juniorwebdev@gmail.com')->send(new ContactMail($request->all()));
            return Mail::to($request->input('email'))->send(new ContactMailGreetings());

        }
        catch(Exception $e) {
            report($e);
            return response()
                ->json(['error' => 'Ocorreu um erro ao enviar a mensagem.'], 401);
        }
    }

    public function sendBudget(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'email|required',
            'phone' => 'required',
            'message' => 'required'
        ]);

        try {

            Mail::to('a.juniorwebdev@gmail.com')->send(new BudgetMail($request->all()));
            return Mail::to($request->input('email'))->send(new BudgetMailGreetings());

        }
        catch(Exception $e) {
            report($e);
            return response()
                ->json(['error' => 'Ocorreu um erro ao enviar a mensagem.'], 401);
        }
    }

    public function sendNewsletter(Request $request)
    {
        $request->validate([
            'email' => 'email|required',
        ]);

        try {
            if(!File::exists(base_path('public\file\newsletter.txt'))) {
                File::put(base_path('public\file\newsletter.txt'), $request->input('email').PHP_EOL);
            }
            else {
                File::append(base_path('public\file\newsletter.txt'), $request->input('email').PHP_EOL);
            }

            Mail::to('a.juniorwebdev@gmail.com')->send(new NewsletterMail($request->all()));
            return Mail::to($request->input('email'))->send(new NewsletterMailGreetings());

        }
        catch(Exception $e) {
            report($e);
            return response()
                ->json(['error' => 'Ocorreu um erro ao enviar a mensagem.'], 401);
        }
    }
}
