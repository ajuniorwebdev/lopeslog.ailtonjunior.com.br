/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/main.js":
/*!******************************!*\
  !*** ./resources/js/main.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

/* Init Carousel */
$('.carousel').carousel();
$('li.nav-item').on('click', function (e) {
  $('li.nav-item').each(function (i, e) {
    $(e).removeClass('active');
  });
  $(e.target).parent().addClass('active');
  var div = $(e.target).attr('data-source');
  var offset = $("".concat(div)).offset().top;
  $(document).scrollTop(offset - 100);
});
/* Menu Active */

var lastId,
    topMenu = $("nav.navbar"),
    topMenuHeight = topMenu.outerHeight() + 100,
    // All list items
menuItems = topMenu.find("div.collapse ul li a"),
    // Anchors corresponding to menu items
scrollItems = menuItems.map(function () {
  var item = $($(this).attr("data-source"));

  if (item.length) {
    return item;
  }
});
$(window).scroll(function () {
  // Get container scroll position
  var fromTop = $(this).scrollTop() + topMenuHeight; // Get id of current scroll item

  var cur = scrollItems.map(function () {
    if ($(this).offset().top < fromTop) return this;
  }); // Get the id of the current element

  cur = cur[cur.length - 1];
  var id = cur && cur.length ? cur[0].id : "";

  if (lastId !== id) {
    lastId = id; // Set/remove active class

    menuItems.parent().removeClass("active").end().filter("[data-source='#" + id + "']").parent().addClass("active");
  }
});
/* Modal */

$('#budget-middle').on('click', function () {
  $('.modal').modal('show');
});
$('#budget-top').on('click', function () {
  $('.modal').modal('show');
});
$('#budget-menu').on('click', function () {
  $('.modal').modal('show');
});
$('#form-contact [name="phone"]').mask('(00) 00000-0000');
/* Contact Send Message */

$('form#form-contact').on('submit', function (e) {
  e.preventDefault();
  $('#form-contact .form-control').each(function (i, e) {
    $(e).removeClass('is-invalid');
    $(e).parent().find('span').addClass('d-none');
  });
  $.ajax({
    url: '/send/contact',
    method: 'post',
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    data: $('form#form-contact').serialize(),
    success: function success(res) {
      // $('form#form-contact')[0].reset()
      Swal.fire({
        icon: 'success',
        title: 'Mensagem Enviada!',
        text: 'Mensagem enviada com sucesso!',
        showCancelButton: false,
        showConfirmButton: true
      });
    },
    error: function error(err) {
      var e = JSON.parse(err.responseText).errors;

      if (e !== undefined) {
        for (i in e) {
          $('#form-contact [name="' + i + '"]').addClass('is-invalid');
          $('#form-contact [name="' + i + '"]').parent().find('span').text(e[i][0]).removeClass('d-none');
        }
      }

      Swal.fire({
        icon: 'error',
        title: 'Atenção',
        text: 'Ocorreu um problema ao enviar sua mensagem, tente novamente mais tarde!',
        showCancelButton: true,
        showConfirmButton: false
      });
    }
  });
});
/* Budget Send Message */

$('div.modal button.send').on('click', function (e) {
  e.preventDefault();
  $('#form-budget .form-control').each(function (i, e) {
    $(e).removeClass('is-invalid');
    $(e).parent().find('span').addClass('d-none');
  });
  $.ajax({
    url: '/send/budget',
    method: 'post',
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    data: $('form#form-budget').serialize(),
    success: function success(res) {
      // $('form#form-contact')[0].reset()
      $('.modal').modal('hide');
      Swal.fire({
        icon: 'success',
        title: 'Solicitação Enviada!',
        text: 'Solicitação de orçamento enviado com sucesso!',
        showCancelButton: false,
        showConfirmButton: true
      });
    },
    error: function error(err) {
      var e = JSON.parse(err.responseText).errors;

      if (e !== undefined) {
        for (i in e) {
          $('#form-budget [name="' + i + '"]').addClass('is-invalid');
          $('#form-budget [name="' + i + '"]').parent().find('span').text(e[i][0]).removeClass('d-none');
        }
      }

      Swal.fire({
        icon: 'error',
        title: 'Atenção',
        text: 'Ocorreu um problema ao enviar sua solicitação, tente novamente mais tarde!',
        showCancelButton: true,
        showConfirmButton: false
      });
    }
  });
});
/* Newsletter Send Message */

$('form#form-newsletter').on('submit', function (e) {
  e.preventDefault();
  $('#form-newsletter .form-control').each(function (i, e) {
    $(e).removeClass('is-invalid');
    $(e).parent().parent().find('span.text-danger').addClass('d-none');
  });
  $.ajax({
    url: '/send/newsletter',
    method: 'post',
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    data: $('form#form-newsletter').serialize(),
    success: function success(res) {
      $('form#form-newsletter')[0].reset();
      Swal.fire({
        icon: 'success',
        title: 'E-mail cadastrado!',
        text: 'Seu e-mail foi cadastrado com sucesso!',
        showCancelButton: false,
        showConfirmButton: true
      });
    },
    error: function error(err) {
      var e = JSON.parse(err.responseText).errors;

      if (e !== undefined) {
        for (i in e) {
          $('#form-newsletter [name="' + i + '"]').addClass('is-invalid');
          $('#form-newsletter [name="' + i + '"]').parent().parent().find('span.text-danger').text(e[i][0]).removeClass('d-none');
        }
      }

      Swal.fire({
        icon: 'error',
        title: 'Atenção',
        text: 'Ocorreu um problema ao fazer seu cadastro, tente novamente mais tarde!',
        showCancelButton: true,
        showConfirmButton: false
      });
    }
  });
});

/***/ }),

/***/ 1:
/*!************************************!*\
  !*** multi ./resources/js/main.js ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\localhost\lopeslog_institucional\resources\js\main.js */"./resources/js/main.js");


/***/ })

/******/ });