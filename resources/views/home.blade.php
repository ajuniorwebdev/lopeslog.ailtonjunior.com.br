<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Lopes Log</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
</head>
<body>

<div class="contact-info">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 text-right">
                <div class="info">
                    <span class="mr-3"><span class="material-icons mr-2">phone</span>(14) 99855-9857</span>
                    <span class="mr-3"><span class="material-icons mr-2">email</span>anderson@lopeslog.com.br</span>
                    <a id="budget-top" class="btn-budget" href="javascript:void(0)">Solicite Orçamento!</a>
                </div>
            </div>
        </div>
    </div>
</div>

<nav id="home" class="navbar navbar-expand-lg navbar-light fixed-top">
    <div class="container">
        <a class="navbar-brand" href="{{ route('home') }}">
            <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 462 135" width="50%" height="100%">
                <title>Logo Lopes Log</title>
                <g>
                    <g>
                        <g>
                            <path d="M411.53,65.26q0,15.14-5.41,22.88t-15.7,7.74q-10.34,0-15.83-7.72t-5.49-22.9q0-15,5.45-22.81t15.83-7.76q10.25,0,15.7,7.72T411.53,65.26Zm-10.25,0q0-10.52-2.44-16.56t-8.54-6q-6.1,0-8.52,6.06t-2.42,16.53q0,10.3,2.38,16.45t8.69,6.15q5.92,0,8.39-6T401.28,65.28Z" fill="#304486"/>
                            <path d="M438.28,64.53H458v30.7h-5.21l-1.3-7.48q-4.49,8.13-14.2,8.13a17.77,17.77,0,0,1-15.45-8q-5.57-8-5.57-21.77,0-14.49,5.51-22.94t16.91-8.45q15.9,0,19,18l-8.82,2.21q-1.55-7.61-4.19-9.86a9.27,9.27,0,0,0-6.21-2.25q-11.87,0-11.87,22.44t12.15,22.57q10.08,0,10.08-15.44H438.28Z" fill="#304486"/>
                            <polygon points="347.64 25.85 337.87 25.21 337.87 94.8 368.52 94.8 368.52 86.54 347.64 86.54 347.64 25.85" fill="#304486"/>
                        </g>
                        <g>
                            <path d="M208,65.26q0,15.14-5.41,22.88t-15.7,7.74q-10.34,0-15.83-7.72t-5.49-22.9q0-15,5.45-22.81t15.83-7.76q10.25,0,15.7,7.72T208,65.26Zm-10.25,0q0-10.52-2.44-16.56t-8.54-6q-6.1,0-8.52,6.06t-2.42,16.53q0,10.3,2.38,16.45t8.69,6.15q5.92,0,8.39-6T197.74,65.28Z" fill="#5aa0c9"/>
                            <path d="M214,35.77h14.62A40.35,40.35,0,0,1,240,37.09a13.49,13.49,0,0,1,7.35,5.84,19.29,19.29,0,0,1,3,10.88,19.74,19.74,0,0,1-2.42,10,14.24,14.24,0,0,1-6.51,6.05q-4.09,1.86-12.56,1.86h-5.06v23H214Zm9.77,7.57V63.92h4.84q6.49,0,9-2.42t2.49-7.87A11.93,11.93,0,0,0,238.41,47a6.89,6.89,0,0,0-3.61-3.11,23.83,23.83,0,0,0-6.21-.58Z" fill="#5aa0c9"/>
                            <path d="M255.08,35.77H288.9V43.6h-24V60.46h18.85v7.87H264.86V86.8h24.43v8H255.08Z" fill="#5aa0c9"/>
                            <path d="M331.1,49l-8.35,2.9a15.76,15.76,0,0,0-3.87-6.93,9.23,9.23,0,0,0-6.55-2.24A7.68,7.68,0,0,0,306.38,45a7.49,7.49,0,0,0-2.1,5.23,6.35,6.35,0,0,0,2.36,5.18Q309,57.31,316,60a52.48,52.48,0,0,1,9.25,4.37A15.73,15.73,0,0,1,332.57,78a17.22,17.22,0,0,1-5.25,12.67q-5.26,5.23-14.73,5.23-16.34,0-20.24-16.52l8.73-2.47q2.29,10.9,11.94,10.9,4.71,0,7.16-2.44a8.2,8.2,0,0,0,2.44-6,8,8,0,0,0-1.69-4.89q-1.69-2.25-8.39-4.8a71,71,0,0,1-10.23-4.54,16.05,16.05,0,0,1-7.55-13.75,15.92,15.92,0,0,1,4.91-11.85q4.91-4.8,13-4.8Q327.12,34.69,331.1,49Z" fill="#5aa0c9"/>
                            <polygon points="144.09 12.4 134.32 11.76 134.32 94.8 164.98 94.8 164.98 86.54 144.09 86.54 144.09 12.4" fill="#5aa0c9"/>
                        </g>
                    </g>
                    <g>
                        <g>
                            <polygon points="28.81 109.57 28.81 3.94 3.65 2.09 3.65 111.16 28.81 109.57" fill="#bfbebe"/>
                            <polygon points="75.86 106.48 75.86 6.87 54.24 5.44 54.24 107.84 75.86 106.48" fill="#bfbebe"/>
                        </g>
                        <polygon points="131.48 125.38 75.86 106.48 54.24 107.84 106.18 127.74 131.48 125.38" fill="#616161"/>
                        <polygon points="77.36 130.44 28.81 109.57 3.65 111.16 45.56 133.41 77.36 130.44" fill="#616161"/>
                    </g>
                </g>
            </svg>
        </a>

        <div class="dropdown menu-container">
            <button id="menu" class="dropdown-toggle menu-button" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="material-icons">menu</span>
            </button>
            <div id="menuList" class="dropdown-menu" aria-labelledby="menuList">
                <a class="dropdown-item active" href="{{ route('home') }}">HOME</a>
                <a class="dropdown-item" href="javascript:void(0)">SOBRE</a>
                <a class="dropdown-item" href="javascript:void(0)">SERVIÇOS</a>
                <a class="dropdown-item" href="javascript:void(0)">CLIENTES</a>
                <a class="dropdown-item" href="javascript:void(0)">CONTATO</a>
                <a id="budget-menu" class="dropdown-item" href="javascript:void(0)">SOLICITAR ORÇAMENTO</a>
            </div>

        </div>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto text-center">
                <li class="nav-item active"><a class="nav-link" href="{{ route('home') }}" data-source="#home">HOME</a></li>
                <li class="nav-item"><a class="nav-link" href="javascript:void(0)" data-source="#about">SOBRE</a></li>
                <li class="nav-item"><a class="nav-link" href="javascript:void(0)" data-source="#services">SERVIÇOS</a></li>
                <li class="nav-item"><a class="nav-link" href="javascript:void(0)" data-source="#clients">CLIENTES</a></li>
                <li class="nav-item"><a class="nav-link" href="javascript:void(0)" data-source="#contact">CONTATO</a></li>
            </ul>
        </div>

        <div class="dropdown contact-list-container">
            <button id="contactList" class="dropdown-toggle contact-list" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="material-icons">more_vert</span>
            </button>
            <div id="contactList" class="dropdown-menu" aria-labelledby="contactList">
                <a class="dropdown-item" href="javascript:void(0)"><span class="material-icons mr-2 py-3">phone</span>(14) 99855-9857</a>
                <a class="dropdown-item" href="javascript:void(0)"><span class="material-icons mr-2 py-3">email</span>anderson@lopeslog.com.br</a>
                <a class="dropdown-item" href="javascript:void(0)"><span class="material-icons mr-2 py-3">location_on</span>Rua Carlos Giaxa 4-48, Prq. Júlio Nobrega<br>Bauru-SP</a>
            </div>
        </div>

    </div>
</nav>

<section id="carousel">
    <div id="carouselContainer" class="carousel slide carousel-fade" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="{{ asset('img/carousel/carrousel-imagem.png') }}" class="d-block w-100">
            </div>
            <div class="carousel-item">
                <img src="{{ asset('img/carousel/locacao-galpao-exp.jpeg') }}" class="d-block w-100">
            </div>
        </div>
    </div>
</section>

<section id="infraestructure">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-4">
                <div class="box-container palete">
                    <div class="black-opacity"></div>
                    <div class="text">
                        <div class="svg">
                        <?xml version = "1.0" encoding = "iso-8859-1"?>
                        <!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                            <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 width="100%" height="100%" viewBox="0 0 612 612" style="enable-background:new 0 0 612 612;" xml:space="preserve" fill="white">
                                    <g>
                                        <path d="M283.221,72.536h-60.658v30.062h-28.336V72.536h-61.03c-22.057,0-39.938,17.882-39.938,39.939v133.177
                                            c0,22.057,17.881,39.938,39.938,39.938h150.024c22.058,0,39.939-17.881,39.939-39.938V112.476
                                            C323.16,90.417,305.279,72.536,283.221,72.536z M225.857,188.333v58.689h-35.45v-58.689c-24.164,0-24.159,0-25.061,0l42.941-50.815
                                            l42.785,51.002C251.072,188.52,250.828,188.333,225.857,188.333z M278.029,539.464H45.714c-9.01,0-17.354-2.695-24.433-7.195
                                            l137.256-84.984l140.372,86.914C292.623,537.452,285.595,539.464,278.029,539.464z M3.745,511.829
                                            C1.351,506.28,0,500.177,0,493.751V367.618c0-5.842,1.198-11.381,3.196-16.518l130.066,80.533L3.745,511.829z M158.537,415.985
                                            L19.745,330.05c7.381-5.113,16.311-8.146,25.969-8.146h232.315c8.231,0,15.845,2.348,22.516,6.156L158.537,415.985z
                                             M183.812,431.634l135.28-83.762c2.893,5.998,4.652,12.641,4.652,19.746v126.133c0,7.682-2.074,14.812-5.423,21.168
                                            L183.812,431.634z M565.94,75.763h-69.954v66.872h-32.68V75.763h-70.384c-25.438,0-46.061,20.622-46.061,46.061v370.955
                                            c0,25.439,20.622,46.061,46.061,46.061H565.94c25.438,0,46.06-20.621,46.06-46.061V121.824
                                            C612,96.385,591.378,75.763,565.94,75.763z M437.723,265.042l32.463,38.699c0,0-10.713,0-23.434,0v50.309h-18.06v-50.309
                                            c-12.31,0-22.751,0-23.435,0L437.723,265.042z M544.144,401.349H414.718v-16.34h129.427L544.144,401.349L544.144,401.349z
                                             M530.17,303.741v50.309h-18.06v-50.309c-12.31,0-22.75,0-23.435,0l32.465-38.699l32.463,38.699
                                            C553.604,303.741,542.892,303.741,530.17,303.741z"/>
                                    </g>
                                <g></g>
                                <g></g>
                                <g></g>
                                <g></g>
                                <g></g>
                                <g></g>
                                <g></g>
                                <g></g>
                                <g></g>
                                <g></g>
                                <g></g>
                                <g></g>
                                <g></g>
                                <g></g>
                                <g></g>
                                </svg>
                        </div>
                        <p><strong>+ 3.000</strong></p>
                        <p>Posições Paletes</p>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 col-lg-4">
                <div class="box-container carga-descarga">
                    <div class="black-opacity"></div>
                    <div class="text">
                        <div class="svg">
                        <?xml version = "1.0" encoding = "iso-8859-1"?>
                        <!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                            <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 width="100%" height="100%" viewBox="0 0 612 612" style="enable-background:new 0 0 612 612;" xml:space="preserve" fill="white">
                                    <g>
                                        <path d="M187.684,87.183H94.005c-15.975,0-28.925,12.95-28.925,28.924v151.854H36.155C16.188,267.961,0,284.149,0,304.117v127.485
                                            c12.438-20.898,35.215-34.854,61.248-34.854c37.674,0,68.624,29.358,71.154,66.454H268.78
                                            c2.532-37.096,33.408-66.454,71.154-66.454c13.305,0,25.815,3.688,36.444,10.052L226.38,110.951
                                            C218.989,96.371,204.03,87.183,187.684,87.183z M281.146,326.099H117.578c-4.049,0-7.23-3.254-7.23-7.23V139.103
                                            c0-3.977,3.182-7.23,7.23-7.23h71.226c2.748,0,5.207,1.519,6.436,3.904l92.342,179.766
                                            C290.04,320.387,286.569,326.099,281.146,326.099z M61.245,411.197c-31.375,0-56.811,25.435-56.811,56.81
                                            c0,31.376,25.436,56.811,56.811,56.811s56.811-25.435,56.811-56.811C118.056,436.631,92.621,411.197,61.245,411.197z
                                             M61.245,496.413c-15.688,0-28.405-12.718-28.405-28.405s12.718-28.405,28.405-28.405S89.65,452.32,89.65,468.007
                                            S76.933,496.413,61.245,496.413z M549.565,434.277v28.925h-138.62c-1.62-24.09-15.226-44.896-34.927-56.537V116.107h28.924v318.169
                                            H549.565L549.565,434.277z M339.914,411.197c-31.375,0-56.811,25.435-56.811,56.81c0,31.376,25.436,56.811,56.811,56.811
                                            s56.811-25.435,56.811-56.811C396.726,436.631,371.29,411.197,339.914,411.197z M339.914,496.413
                                            c-15.688,0-28.405-12.718-28.405-28.405s12.718-28.405,28.405-28.405s28.405,12.718,28.405,28.405S355.602,496.413,339.914,496.413
                                            z M584.724,195.462h-48.657v49.919h-41.045v-49.919h-48.912c-15.064,0-27.275,12.213-27.275,27.276v170.495
                                            c0,15.064,12.212,27.276,27.275,27.276h138.614c15.064,0,27.276-12.212,27.276-27.276V222.739
                                            C612,207.675,599.787,195.462,584.724,195.462z"/>
                                    </g>
                                <g></g>
                                <g></g>
                                <g></g>
                                <g></g>
                                <g></g>
                                <g></g>
                                <g></g>
                                <g></g>
                                <g></g>
                                <g></g>
                                <g></g>
                                <g></g>
                                <g></g>
                                <g></g>
                                <g></g>
                                </svg>
                        </div>
                        <p><strong>Carga e Descarga</strong></p>
                        <p>Com portão fechado</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-lg-4">
                <div class="box-container ambiente">
                    <div class="black-opacity"></div>
                    <div class="text">
                        <div class="svg">
                        <?xml version = "1.0" encoding = "iso-8859-1"?>
                        <!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                            <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 width="100%" height="100%" viewBox="0 0 612 612" style="enable-background:new 0 0 612 612;" xml:space="preserve" fill="white">
                                    <g>
                                        <path d="M484.338,360.237l-12.01,6.304l-12.011-6.304c-71.126-37.82-152.815-92.762-153.24-182.713v-47.02h-32.624v63.886h-60.138
                                            v-63.886H84.754C37.991,130.504,0,168.495,0,215.344v282.63c0,46.85,37.991,84.755,84.754,84.755h318.492
                                            c46.765,0,84.755-37.905,84.755-84.755V358.277C486.723,358.959,485.531,359.556,484.338,360.237z M99.406,384.769l32.028,38.162
                                            c0,0-10.562,0-23.084,0v49.574H90.548v-49.574h-23.17L99.406,384.769z M204.349,519.185H76.748v-16.1h127.6L204.349,519.185
                                            L204.349,519.185z M190.635,422.931v49.574h-17.888v-49.574h-23.084l32.027-38.162l32.028,38.162
                                            C213.719,422.931,203.156,422.931,190.635,422.931z M472.313,29.271c-34.21,26.831-81.071,42.17-139.687,42.17v106.003
                                            c0.316,68.21,56.823,116.178,139.687,160.192c82.811-44.015,139.37-91.983,139.687-160.192V71.439
                                            C553.384,71.439,506.523,56.101,472.313,29.271z M472.313,313.651V177.443H353.711V91.892
                                            c46.176-2.846,85.921-15.128,118.602-36.688v122.239h118.602C590.652,227.046,554.966,268.108,472.313,313.651z"/>
                                    </g>
                                <g></g>
                                <g></g>
                                <g></g>
                                <g></g>
                                <g></g>
                                <g></g>
                                <g></g>
                                <g></g>
                                <g></g>
                                <g></g>
                                <g></g>
                                <g></g>
                                <g></g>
                                <g></g>
                                <g></g>
                                </svg>
                        </div>
                        <p><strong>Ambiente Seguro</strong></p>
                        <p>Cuidamos para você</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="about">
    <div class="about-box">
        <div class="container">
            <div class="row title-section">
                <div class="col-md-12">
                    <h2>SOBRE</h2>
                    <hr class="float-left">
                </div>
            </div>
            <div class="row about-text">
                <div class="col-sm-12 col-lg-6">
                    <p>A <strong>LOPES LOG</strong> é uma empresa que atua no gerenciamento de estoque de terceiros, fundada em 2018 para poder atender
                        as necessidades de uma indústria de origem Alemã do segmento alimentício e pioneira no Brasil.</p>
                    <p>Está situada no centro do estado de São Paulo, contando com o privilégio da malha rodoviária que ligam o estado de SP
                        com RJ, MG, MS e PR.</p>
                    <p>Desenvolvemos soluções customizadas e de melhoria contínua de acordo com os desafios de cada cliente.</p>
                </div>
                <div class="col-sm-12 col-lg-6">
                    <div class="box-img">
                        <img src="{{ asset('img/locacao-galpao-exp.jpeg') }}" class="w-100">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="about-values">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-4">
                    <div class="icon">
                        <img src="{{ asset('img/icons-svg/target.svg') }}">
                    </div>
                    <div class="box">
                        <h2 class="title">MISSÃO</h2>
                        <div class="text">
                            <p>Embalar e Armazenar produtos e materiais.</p>
                            <p>Garantir a excelência no armazenamento e na embalagem de produtos zelando da sua integridade.</p>
                            <p>Gerenciar estoque com eficiência e qualidade.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-lg-4">
                    <div class="icon">
                        <img src="{{ asset('img/icons-svg/spyglass.svg') }}">
                    </div>
                    <div class="box">
                        <h2 class="title">VISÃO</h2>
                        <div class="text">
                            <p>Ser reconhecida pelos nossos clientes como referência de excelência na qualidade em armazenagem e embalagem.</p>
                            <p>Expandir nossa área de armazenagem para que possamos atender a diversas empresas que atuam e ou transitam por na nossa região.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-lg-4">
                    <div class="icon">
                        <img src="{{ asset('img/icons-svg/value.svg') }}">
                    </div>
                    <div class="box">
                        <h2 class="title">VALORES</h2>
                        <div class="text">
                            <p>Dedicação aos clientes</p>
                            <p>Integridade</p>
                            <p>Foco em Resultado</p>
                            <p>Eficiência</p>
                            <p>Nossas pessoas</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>

<div class="budget">
    <div class="budget-box">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-lg-8">
                    <div class="title py-3">
                        <h2>Armazenamos para você com qualidade e segurança.</h2>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-4">
                    <div class="button">
                        <button id="budget-middle">SOLICITE ORÇAMENTO</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<section id="services">
    <div class="container">
        <div class="row title-section">
            <div class="col-md-12">
                <h2>SERVIÇOS</h2>
                <hr>
            </div>
        </div>

        <div class="row box-services">
            <div class="col-sm-12 col-lg-6">
                <div class="box-service">
                    <div class="row">
                        <div class="col-sm-12 col-lg-3">
                            <div class="service-img">
                                <img src="{{ asset('img/armazenagem.jpg') }}" class="w-100">
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-9">
                            <div class="service-text">
                                <h2 class="title">Armazenagem</h2>
                                <p>Infraestrutura adaptável para atender os requisitos de cada cliente e produto, atuando no modo multicliente e dedicada, com monitoramento de temperatura, umidade e pragas.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-lg-6">
                <div class="box-service">
                    <div class="row">
                        <div class="col-sm-12 col-lg-3">
                            <div class="service-img">
                                <img src="{{ asset('img/operacoes.jpg') }}" class="w-100">
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-9">
                            <div class="service-text">
                                <h2 class="title">Operações In House</h2>
                                <p>Customizamos nossos projetos logísticos agregando mão-de-obra treinada e qualificada às necessidades da sua empresa.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-lg-6">
                <div class="box-service">
                    <div class="row">
                        <div class="col-sm-12 col-lg-3">
                            <div class="service-img">
                                <img src="{{ asset('img/op_picking.jpg') }}" class="w-100">
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-9">
                            <div class="service-text">
                                <h2 class="title">Operações de Picking e Reembalagens</h2>
                                <p>Projetos construídos de forma especial para melhor atendê-lo.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-lg-6">
                <div class="box-service">
                    <div class="row">
                        <div class="col-sm-12 col-lg-3">
                            <div class="service-img">
                                <img src="{{ asset('img/gestao_processos.jpg') }}" class="w-100">
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-9">
                            <div class="service-text">
                                <h2 class="title">Gestão de Processos de Distribuição</h2>
                                <p>Operações Cross Docking para fracionamento de cargas e distribuição.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-lg-6">
                <div class="box-service">
                    <div class="row">
                        <div class="col-sm-12 col-lg-3">
                            <div class="service-img">
                                <img src="{{ asset('img/gestao_estoque.jpg') }}" class="w-100">
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-9">
                            <div class="service-text">
                                <h2 class="title">Gestão de Estoque</h2>
                                <p>Otimização de estoque, processos de faturamento e gerenciamento de pedidos.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-lg-6">
                <div class="box-service">
                    <div class="row">
                        <div class="col-sm-12 col-lg-3">
                            <div class="service-img">
                                <img src="{{ asset('img/outros_servicos.jpg') }}" class="w-100">
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-9">
                            <div class="service-text">
                                <h2 class="title">Outros Serviços</h2>
                                <p>Copacking, Etiquetagem, Embalagem e Customização de Kit’s.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="quality-services">
    <div class="quality-services-box">
        <div class="container">
            <div class="row title-section">
                <div class="col-md-12">
                    <h2>PADRÕES E QUALIDADES</h2>
                    <hr class="float-left">
                </div>
            </div>
            <div class="row py-3 quality-services">
                <div class="col-sm-12 col-lg-6 quality-img">
                    <img src="{{ asset('img/orkin.png') }}" alt="Orkin" class="w-100">
                </div>
                <div class="col-sm-12 col-lg-6">
                    <div class="quality-list">
                        <ul>
                            <li><img src="{{ asset('img/icons-svg/check.svg') }}">Armazenagem Seca</li>
                            <li><img src="{{ asset('img/icons-svg/check.svg') }}">Seguro</li>
                            <li><img src="{{ asset('img/icons-svg/check.svg') }}">Controle Monitorado Pragas - <strong>ORKIN</strong></li>
                            <li><img src="{{ asset('img/icons-svg/check.svg') }}">Alarme Monitorador / Câmeras</li>
                            <li><img src="{{ asset('img/icons-svg/check.svg') }}">Colaboradores Uniformizados</li>
                            <li><img src="{{ asset('img/icons-svg/check.svg') }}">BPA - Manual de Boas Práticas</li>
                            <li><img src="{{ asset('img/icons-svg/check.svg') }}">Controle de acessos de pessoas</li>
                            <li><img src="{{ asset('img/icons-svg/check.svg') }}">SLA's de qualidade e eficiência</li>
                            <li><img src="{{ asset('img/icons-svg/check.svg') }}">Check-list de inspeção de mercadoria</li>
                            <li><img src="{{ asset('img/icons-svg/check.svg') }}">Romaneio de carga</li>
                            <li><img src="{{ asset('img/icons-svg/check.svg') }}">Recebimento / Expedição paletizado</li>
                            <li><img src="{{ asset('img/icons-svg/check.svg') }}">Altura máxima Palete 1.500mm</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="clients">
    <div class="client-box">
        <div class="container">
            <div class="row title-section">
                <div class="col-md-12">
                    <h2>CLIENTES</h2>
                    <hr>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-lg-6">
                    <div class="client-img">
                        <img src="{{ asset('img/haribo.jpg') }}" alt="Haribo" class="w-100">
                    </div>
                </div>
                <div class="col-sm-12 col-lg-6">
                    <div class="client-list">
                        <ul>
                            <li><img src="{{ asset('img/icons-svg/check.svg') }}">Empresa de origem Alemã.</li>
                            <li><img src="{{ asset('img/icons-svg/check.svg') }}">Iniciou suas atividades no Brasil em 2016.</li>
                            <li><img src="{{ asset('img/icons-svg/check.svg') }}">A Haribo Brasil já possui altas certificações de qualidade produtiva e segurança do trabalho, como a FSSC 22000, SMETA e Certificação de Qualidade Brasil.</li>
                            <li><img src="{{ asset('img/icons-svg/check.svg') }}">Armazena seus produtos e embalagens no Lopes Log desde 2018.</li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section id="contact">
    <div class="contact-box">
        <div class="container">
            <div class="row title-section">
                <div class="col-md-12">
                    <h2>FALE CONOSCO</h2>
                    <hr class="float-left">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-lg-6">
                    <form id="form-contact">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="name" placeholder="Nome" autocomplete="off">
                                    <span class="d-none text-danger"></span>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group">
                                    <input type="email" class="form-control" name="email" placeholder="E-mail" autocomplete="off">
                                    <span class="d-none text-danger"></span>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group">
                                    <input type="phone" class="form-control" name="phone" placeholder="Telefone" autocomplete="off">
                                    <span class="d-none text-danger"></span>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <textarea class="form-control" name="message" id="message" cols="30" rows="10" placeholder="Mensagem" autocomplete="off"></textarea>
                                    <span class="d-none text-danger"></span>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <button type="submit">ENVIAR</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="contact-address-box col-md-12 col-lg-6">
                    <div class="row title-section">
                        <div class="col-md-12">
                            <h2>UNIDADE DE BAURU</h2>
                            <hr class="float-left">
                        </div>
                    </div>
                    <div class="contact-address">
                        <p><span class="material-icons mr-2">location_on</span>Rua Carlos Giaxa, 4-48, Prq. Júlio Nobrega, 17031-421, Bauru-SP</p>
                        <p><span class="material-icons mr-2">phone</span>(14) 99855-9857</p>
                        <p><span class="material-icons mr-2">email</span>anderson@lopeslog.com.br</p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section id="map">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3691.028891649481!2d-49.02631297056865!3d-22.31474706550741!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94bf5d7428f68d81%3A0x80b2b0a6fd4aade2!2sR.%20Carlos%20Giaxa%20-%20Parque%20Julio%20Nobrega%2C%20Bauru%20-%20SP%2C%2017031-421!5e0!3m2!1spt-BR!2sbr!4v1602516425011!5m2!1spt-BR!2sbr" width="100%" height="100%" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
</section>

<footer>
    <div class="footer-box">
        <div class="container">
            <div class="row">
                <div class="footer-img col-sm-12 col-lg-4">
                    <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 462 135">
                        <title>logo-hor-white</title>
                        <g>
                            <g>
                                <g>
                                    <path d="M411.53,65.26q0,15.14-5.41,22.88t-15.7,7.74q-10.34,0-15.83-7.72t-5.49-22.9q0-15,5.45-22.81t15.83-7.76q10.25,0,15.7,7.72T411.53,65.26Zm-10.25,0q0-10.52-2.44-16.56t-8.54-6q-6.1,0-8.52,6.06t-2.42,16.53q0,10.3,2.38,16.45t8.69,6.15q5.92,0,8.39-6T401.28,65.28Z" fill="#fff"/>
                                    <path d="M438.28,64.53H458v30.7h-5.21l-1.3-7.48q-4.49,8.13-14.2,8.13a17.77,17.77,0,0,1-15.45-8q-5.57-8-5.57-21.77,0-14.49,5.51-22.94t16.91-8.45q15.9,0,19,18l-8.82,2.21q-1.55-7.61-4.19-9.86a9.27,9.27,0,0,0-6.21-2.25q-11.87,0-11.87,22.44t12.15,22.57q10.08,0,10.08-15.44H438.28Z" fill="#fff"/>
                                    <polygon points="347.64 25.85 337.87 25.21 337.87 94.8 368.52 94.8 368.52 86.54 347.64 86.54 347.64 25.85" fill="#fff"/>
                                </g>
                                <g>
                                    <path d="M208,65.26q0,15.14-5.41,22.88t-15.7,7.74q-10.34,0-15.83-7.72t-5.49-22.9q0-15,5.45-22.81t15.83-7.76q10.25,0,15.7,7.72T208,65.26Zm-10.25,0q0-10.52-2.44-16.56t-8.54-6q-6.1,0-8.52,6.06t-2.42,16.53q0,10.3,2.38,16.45t8.69,6.15q5.92,0,8.39-6T197.74,65.28Z" fill="#fff"/>
                                    <path d="M214,35.77h14.62A40.35,40.35,0,0,1,240,37.09a13.49,13.49,0,0,1,7.35,5.84,19.29,19.29,0,0,1,3,10.88,19.74,19.74,0,0,1-2.42,10,14.24,14.24,0,0,1-6.51,6.05q-4.09,1.86-12.56,1.86h-5.06v23H214Zm9.77,7.57V63.92h4.84q6.49,0,9-2.42t2.49-7.87A11.93,11.93,0,0,0,238.41,47a6.89,6.89,0,0,0-3.61-3.11,23.83,23.83,0,0,0-6.21-.58Z" fill="#fff"/>
                                    <path d="M255.08,35.77H288.9V43.6h-24V60.46h18.85v7.87H264.86V86.8h24.43v8H255.08Z" fill="#fff"/>
                                    <path d="M331.1,49l-8.35,2.9a15.76,15.76,0,0,0-3.87-6.93,9.23,9.23,0,0,0-6.55-2.24A7.68,7.68,0,0,0,306.38,45a7.49,7.49,0,0,0-2.1,5.23,6.35,6.35,0,0,0,2.36,5.18Q309,57.31,316,60a52.48,52.48,0,0,1,9.25,4.37A15.73,15.73,0,0,1,332.57,78a17.22,17.22,0,0,1-5.25,12.67q-5.26,5.23-14.73,5.23-16.34,0-20.24-16.52l8.73-2.47q2.29,10.9,11.94,10.9,4.71,0,7.16-2.44a8.2,8.2,0,0,0,2.44-6,8,8,0,0,0-1.69-4.89q-1.69-2.25-8.39-4.8a71,71,0,0,1-10.23-4.54,16.05,16.05,0,0,1-7.55-13.75,15.92,15.92,0,0,1,4.91-11.85q4.91-4.8,13-4.8Q327.12,34.69,331.1,49Z" fill="#fff"/>
                                    <polygon points="144.09 12.4 134.32 11.76 134.32 94.8 164.98 94.8 164.98 86.54 144.09 86.54 144.09 12.4" fill="#fff"/>
                                </g>
                            </g>
                            <g>
                                <g>
                                    <polygon points="28.81 109.57 28.81 3.94 3.65 2.09 3.65 111.16 28.81 109.57" fill="#fff"/>
                                    <polygon points="75.86 106.48 75.86 6.87 54.24 5.44 54.24 107.84 75.86 106.48" fill="#fff"/>
                                </g>
                                <polygon points="131.48 125.38 75.86 106.48 54.24 107.84 106.18 127.74 131.48 125.38" fill="#fff"/>
                                <polygon points="77.36 130.44 28.81 109.57 3.65 111.16 45.56 133.41 77.36 130.44" fill="#fff"/>
                            </g>
                        </g>
                    </svg>
                    <p>Armazenamos para você com qualidade e segurança.</p>
                    <form id="form-newsletter">
                        <label>SE INSCREVA EM NOSSA NEWSLETTER</label>
                        <div class="input-group mb-3">
                            <input name="email" type="text" class="form-control" placeholder="Informe Seu E-mail" aria-label="Recipient's username" aria-describedby="basic-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="submit"><span class="material-icons">arrow_right_alt</span></button>
                            </div>
                        </div>
                        <span class="d-none text-danger"></span>
                    </form>
                </div>
                <div class="col-sm-12 col-lg-4">
                    <div class="usefull-links">
                        <div class="title">
                            <h3>LINKS UTEIS</h3>
                        </div>
                        <ul>
                            <li><a href="{{ route('home') }}"><span class="material-icons">keyboard_arrow_right</span>Home</a></li>
                            <li><a href="#about"><span class="material-icons">keyboard_arrow_right</span>Sobre</a></li>
                            <li><a href="#services"><span class="material-icons">keyboard_arrow_right</span>Serviços</a></li>
                            <li><a href="#clients"><span class="material-icons">keyboard_arrow_right</span>Clientes</a></li>
                            <li><a href="#contact"><span class="material-icons">keyboard_arrow_right</span>Contatos</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-4">
                    <div class="footer-contact">
                        <div class="title">
                            <h3>CONTATO</h3>
                        </div>
                        <p><span class="material-icons mr-2">location_on</span>Rua Carlos Giaxa, 4-48, Prq. Júlio Nobrega, 17031-421, Bauru-SP</p>
                        <p><span class="material-icons mr-2">phone</span>(14) 99855-9857</p>
                        <p><span class="material-icons mr-2">email</span>anderson@lopeslog.com.br</p>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-12">
                    <div class="footer-copyright">
                        <hr>
                        <div class="copyright">
                            <span>Copyright © 2020. Desenvolvido por <a href="https://ailtonjunior.com.br" target="_blank">Ailton Junior Web Dev</a>. Todos os direitos reservados</span>
                        </div>
                        <div class="social-media"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="modal" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Solicitar Orçamento</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form-budget">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text" class="form-control" name="name" placeholder="Nome" autocomplete="off">
                                <span class="d-none text-danger"></span>
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-6">
                            <div class="form-group">
                                <input type="email" class="form-control" name="email" placeholder="E-mail" autocomplete="off">
                                <span class="d-none text-danger"></span>
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-6">
                            <div class="form-group">
                                <input type="phone" class="form-control" name="phone" placeholder="Telefone" autocomplete="off">
                                <span class="d-none text-danger"></span>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <textarea class="form-control" name="message" id="message" cols="30" rows="10" placeholder="Em poucas palavras, explique o que deseja armazenar" autocomplete="off"></textarea>
                                <span class="d-none text-danger"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                <button type="button" class="btn btn-primary send">Enviar</button>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>
</body>
</html>
