/* Init Carousel */
$('.carousel').carousel()

$('li.nav-item').on('click', (e) => {
    $('li.nav-item').each((i, e) => {
        $(e).removeClass('active')
    })
    $(e.target).parent().addClass('active')

    let div = $(e.target).attr('data-source')
    let offset = $(`${div}`).offset().top

    $(document).scrollTop(offset - 100)
})

/* Menu Active */
let lastId,
    topMenu = $("nav.navbar"),
    topMenuHeight = topMenu.outerHeight() + 100,
    // All list items
    menuItems = topMenu.find("div.collapse ul li a"),
    // Anchors corresponding to menu items
    scrollItems = menuItems.map(function(){
        let item = $($(this).attr("data-source"))
        if (item.length) { return item }
    })

$(window).scroll(function() {
    // Get container scroll position
    let fromTop = $(this).scrollTop() + topMenuHeight

    // Get id of current scroll item
    let cur = scrollItems.map(function() {
        if ($(this).offset().top < fromTop)
            return this
    });

    // Get the id of the current element
    cur = cur[cur.length-1]
    let id = cur && cur.length ? cur[0].id : ""

    if (lastId !== id) {
        lastId = id;
        // Set/remove active class
        menuItems.parent().removeClass("active")
            .end().filter("[data-source='#"+id+"']").parent().addClass("active");
    }
});

/* Modal */
$('#budget-middle').on('click', () => {
    $('.modal').modal('show')
})

$('#budget-top').on('click', () => {
    $('.modal').modal('show')
})

$('#budget-menu').on('click', () => {
    $('.modal').modal('show')
})

$('#form-contact [name="phone"]').mask('(00) 00000-0000')

/* Contact Send Message */
$('form#form-contact').on('submit', (e) => {
    e.preventDefault()

    $('#form-contact .form-control').each((i,e) => {
        $(e).removeClass('is-invalid')
        $(e).parent().find('span').addClass('d-none')
    })

    $.ajax({
        url: '/send/contact',
        method: 'post',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: $('form#form-contact').serialize(),
        success: (res) => {
            // $('form#form-contact')[0].reset()
            Swal.fire({
                icon: 'success',
                title: 'Mensagem Enviada!',
                text: 'Mensagem enviada com sucesso!',
                showCancelButton: false,
                showConfirmButton: true
            });
        },
        error: (err) => {
            let e = JSON.parse(err.responseText).errors;
            if(e !== undefined) {
                for(i in e) {
                    $('#form-contact [name="'+i+'"]').addClass('is-invalid')
                    $('#form-contact [name="'+i+'"]').parent().find('span').text(e[i][0]).removeClass('d-none')
                }
            }
            Swal.fire({
                icon: 'error',
                title: 'Atenção',
                text: 'Ocorreu um problema ao enviar sua mensagem, tente novamente mais tarde!',
                showCancelButton: true,
                showConfirmButton: false
            });
        }
    })
})

/* Budget Send Message */
$('div.modal button.send').on('click', (e) => {
    e.preventDefault()

    $('#form-budget .form-control').each((i,e) => {
        $(e).removeClass('is-invalid')
        $(e).parent().find('span').addClass('d-none')
    })

    $.ajax({
        url: '/send/budget',
        method: 'post',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: $('form#form-budget').serialize(),
        success: (res) => {
            // $('form#form-contact')[0].reset()
            $('.modal').modal('hide')
            Swal.fire({
                icon: 'success',
                title: 'Solicitação Enviada!',
                text: 'Solicitação de orçamento enviado com sucesso!',
                showCancelButton: false,
                showConfirmButton: true
            });
        },
        error: (err) => {
            let e = JSON.parse(err.responseText).errors;
            if(e !== undefined) {
                for(i in e) {
                    $('#form-budget [name="'+i+'"]').addClass('is-invalid')
                    $('#form-budget [name="'+i+'"]').parent().find('span').text(e[i][0]).removeClass('d-none')
                }
            }
            Swal.fire({
                icon: 'error',
                title: 'Atenção',
                text: 'Ocorreu um problema ao enviar sua solicitação, tente novamente mais tarde!',
                showCancelButton: true,
                showConfirmButton: false
            });
        }
    })
})

/* Newsletter Send Message */
$('form#form-newsletter').on('submit', (e) => {
    e.preventDefault()

    $('#form-newsletter .form-control').each((i,e) => {
        $(e).removeClass('is-invalid')
        $(e).parent().parent().find('span.text-danger').addClass('d-none')
    })

    $.ajax({
        url: '/send/newsletter',
        method: 'post',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: $('form#form-newsletter').serialize(),
        success: (res) => {
            $('form#form-newsletter')[0].reset()
            Swal.fire({
                icon: 'success',
                title: 'E-mail cadastrado!',
                text: 'Seu e-mail foi cadastrado com sucesso!',
                showCancelButton: false,
                showConfirmButton: true
            });
        },
        error: (err) => {
            let e = JSON.parse(err.responseText).errors;
            if(e !== undefined) {
                for(i in e) {
                    $('#form-newsletter [name="'+i+'"]').addClass('is-invalid')
                    $('#form-newsletter [name="'+i+'"]').parent().parent().find('span.text-danger').text(e[i][0]).removeClass('d-none')
                }
            }
            Swal.fire({
                icon: 'error',
                title: 'Atenção',
                text: 'Ocorreu um problema ao fazer seu cadastro, tente novamente mais tarde!',
                showCancelButton: true,
                showConfirmButton: false
            });
        }
    })
})


