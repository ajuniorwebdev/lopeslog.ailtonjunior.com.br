<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::post('/send/contact', 'HomeController@sendContact');
Route::post('/send/budget', 'HomeController@sendBudget');
Route::post('/send/newsletter', 'HomeController@sendNewsletter');
